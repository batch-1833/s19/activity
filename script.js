console.log("Hello World!");


/*
3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)

4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…


*/

let getCube = [2];

let Numbers = getCube.map(getCube =>{
return getCube ** 3;
})

message =`The cube of ` + getCube[0] + ` is ` + Numbers ;
console.log(`${message}`);


/*5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

const address = ["258", "Washington Ave", "NW", "California", "90011"];

// Pre-Array Destructuring
// console.log(address[0]); 
// console.log(address[1]); 
// console.log(address[2]);
// console.log(address[3]); 
// console.log(address[4]);  


console.log(`I live at ${address[0]} ${address[1]} ${address[2]}, ${address[3]} ${address[4]}`);


/*7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.*/

const animal ={

	name: "Lolong",
	type: "saltwater",
	weight: "1075",
	feet: "20",
	inches: "3"
};

// Pre-Object Destructuring
// console.log(animal.name);
// console.log(animal.type);
// console.log(animal.weight);
// console.log(animal.feet);
// console.log(animal.inches);

// let {animal.name, animal.type, animal.weight, animal.feet, animal.inches} = animal;

console.log(`${animal.name} was a ${animal.type} crocodile. He weighed at ${animal.weight} kgs 
	with a measurement of ${animal.feet} ft ${animal.inches} in.`);


// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.


let numbers = [1, 2, 3, 4, 5];
console.log(numbers);

numbers.forEach((number) => {
	console.log(number);

});
// console.log(numbers[4]**3);
const product = (x,y) => x * y;
// console.log(product);
let x = [numbers[4]];
let total = product(x,3);
console.log(`${total}`);

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = [1, 2, 3, 4, 5];

let sum = reduceNumber.reduce((accumulator, currentValue) => {
  return accumulator + currentValue;
});
console.log(sum);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.


class Dog{

	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

    // 13. Create/instantiate a new object from the class Dog and console log the object.

    const Dogs = new Dog();

    Dogs.name = "Frankie";
    Dogs.age = "5";
    Dogs.breed = "Miniature Dachshund";

    console.log(Dogs);



